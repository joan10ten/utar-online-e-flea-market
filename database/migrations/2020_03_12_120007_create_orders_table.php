<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('orders', function (Blueprint $table) {
              $table->increments('id');
              $table->string('order_number')->unique();
              $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->enum('status', ['Pending', 'Processing', 'Completed', 'Decline'])->default('Pending');
              $table->decimal('grand_total', 20, 2);
              $table->unsignedInteger('item_count');
              $table->boolean('payment_status')->default(0);
            $table->string('payment_method')->nullable();
            $table->string('first_name');
              $table->string('last_name');
              $table->string('phone_number');
              $table->string('transaction_date');
              $table->string('address')->nullable();
              $table->timestamps();
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
