@extends('layouts.auth')


@section('content')

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <script type="text/javascript">

    </script>
    <style>
        .btn.btn-back {
            margin-top: 20px;
            margin-bottom: 15px;
            margin-left: 20px;
        }

        .header-text {
            margin-top: 10px;
            margin-left: 20px;
            margin-bottom: 5px;
            font-size: 30px;
            font-family: 'Raleway', sans-serif;
            font-weight: bold;
        }

        .row.input-pic {
            margin-left: 8px;
            margin-bottom: 30px
        }

        .btn-update {
            margin-left: 13px;
            margin-bottom: 40px;
        }

        hr {
            margin-bottom: 20px;
        }
    </style>
</head>

<div class="container">
    <div class="row">
        <a class="btn btn-back btn-primary" href="{{ route('orders.index') }}"> Back</a>
    </div>

    <div class="row">
        <h3 class="header-text"> Edit Order </h3>
    </div>

    <hr>

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form action="{{ route('orders.update',$order->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')


        <div class="row">
            <div class="form-group col-md-6">
                <strong>Order Number:</strong>
                <p>{{ $order->order_number }} </p>
                <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>

            <div class="form-group col-md-12">
                <strong>Payment Status:</strong>
                <select class='form-control select' name='payment_status'>
                  <option value="0" selected="">Unpaid</option>
                  <option value="1" selected="">Paid</option>
                </select>
                <span class="text-danger">{{ $errors->first('payment_status') }}</span>
            </div>

            <!-- <div class="form-group col-md-12">
                <strong>Supplier ID:</strong>
                <input type="number" name="supplier_id" value="{{ $order->supplier_id }}" class="form-control" placeholder="Supplier ID">
            </div> -->


            <div class="form-group col-md-12">
                <strong>Order Status:</strong>
                <select class='form-control select' name='status'>
                  <option value="Pending" selected="">Pending</option>
                  <option value="Processing" selected="">Processing</option>
                  <option value="Completed" selected="">Completed</option>
                  <option value="Decline" selected="">Decline</option>
                </select>
                <span class="text-danger">{{ $errors->first('unit') }}</span>
            </div>

            <button type="submit" class="btn btn-update btn-success col-sm-2">Update</button>
        </div>
    </form>
</div>
@endsection
