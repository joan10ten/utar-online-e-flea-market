@extends('layouts.auth')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Show Order</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('orders.index') }}"> Back</a>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Order Number:</strong>
            {{ $order->order_number}}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Payment Status:</strong>
            {{ $order->payment_status}}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Status:</strong>
            {{ $order->status}}
        </div>
    </div>
    @foreach ( $order->items as $item)
    <div class="col-xs-18 col-sm-6 col-md-3">
        <div class="thumbnail">
          <td width="1%"><img src="data:image/jpeg;base64,{{$item->product->image}}" width="150px" height="150px" /></td>
            <div class="caption">
            <h4><a href="{{route('products.show',$item->product_id) }}">{{$item->product->name}} </a></h4>
            <p><strong>Quantity: </strong> {{$item->quantity}}</p>


                <p><strong>Category: </strong> {{$item->product->category->name}}</p>
            </div>
        </div>
    </div>

@endforeach
</div>
@endsection
