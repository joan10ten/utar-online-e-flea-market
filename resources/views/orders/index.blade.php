@extends('layouts.auth')

@section('content')
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Utar E-Flea Market Inventory Management System</h2>
            </div>

        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Order number</th>
            <th>Order date</th>
            <th>Status</th>

            <th width="280px">Action</th>
        </tr>

        @foreach ($orders->sortByDesc('updated_at') as $order)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{ $order->order_number }}</td>
            <td>{{ $order->transaction_date }}</td>
            <td>{{ $order->status }}</td>

            <td>

                  <a class="btn btn-info" href="{{ route('orders.show',$order->id) }}">Show</a>

                    <a class="btn btn-primary" href="{{ route('orders.edit',$order->id) }}">Edit</a>


                </form>
            </td>

        </tr>

        @endforeach

    </table>


@endsection
