@extends('layouts.auth')

@section('content')

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <style>
        .btn.btn-back {
            margin-top: 20px;
            margin-bottom: 15px;
            margin-left: 20px;
        }

        .header-text {
            margin-top: 10px;
            margin-left: 20px;
            margin-bottom: 5px;
            font-size: 30px;
            font-family: 'Raleway', sans-serif;
            font-weight: bold;
        }

        .btn-add {
            margin-top: 10px;
            margin-left: 13px;
            margin-bottom: 40px;
        }

        hr {
            margin-bottom: 20px;
        }
    </style>
</head>
<div class="container">
    <div class="row">
        <a class="btn btn-back btn-primary" href="{{ route('users.index') }}"> Back</a>
    </div>

    <div class="row">
        <h3 class="header-text"> Add New Member </h3>
    </div>


    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form action="{{ route('users.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group col-md-12">
                        <strong>Name:</strong>
                        <input type="text" name="name" class="form-control" placeholder="Name" required focus>
        				<span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>
        <div class="form-group col-md-12">
                <strong>Email:</strong>
                <input type="text" name="email" class="form-control" placeholder="Email" required focus>
				<span class="text-danger">{{ $errors->first('email') }}</span>
            </div>
            <div class="form-group col-md-12">
                  <strong>Password:</strong>
                  <input id="password" type="password" class="form-control" name="password" required autocomplete="new-password" placeholder="Password" required focus>
                  <!-- <input type="password" name="password1" class="form-control" placeholder="Password"> -->

              </div>
              <div class="form-group col-md-12">
                <strong>Confirm Password:</strong>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password" required focus>
                <!-- <input type="password" name="password2" class="form-control" placeholder="Confirm Password"> -->
				<span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
            </div>

            <button type="submit" class="btn btn-add btn-success col-sm-2">Add</button>
        </div>
    </form>
</div>

@endsection
