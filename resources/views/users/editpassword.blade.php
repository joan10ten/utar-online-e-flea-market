@extends('layouts.auth')

@section('content')

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
</head>
<style>
    .btn.btn-back {
        margin-top: 20px;
        margin-bottom: 15px;
        margin-left: 20px;
    }

    .header-text {
        margin-top: 10px;
        margin-left: 20px;
        margin-bottom: 5px;
        font-size: 30px;
        font-family: 'Raleway', sans-serif;
        font-weight: bold;
    }

    .btn-update {
        margin-left: 1px;
        margin-bottom: 40px;
    }

    hr {
        margin-bottom: 20px;
    }
</style>

<div class="container">
    <div class="row">
        <a class="btn btn-back btn-primary" href="{{ route('users.index') }}"> Back</a>
    </div>

    <div class="row">
        <h3 class="header-text"> Change Password </h3>
    </div>

    <hr>

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif




    <form action="{{ route('users.updatepassword',$user->id) }}" method="POST">

        @csrf
        @method('PUT')


    <form action="{{ route('users.updatepassword',$user->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="row">
            <div class="form-group col-md-12">
                <strong>New Password:</strong>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
				<span class="text-danger">{{ $errors->first('password') }}</span>
                <!-- <input type="password" name="password" value="{{ $user->password }}" class="form-control" placeholder="Password"> -->
            </div>


            <div class="form-group col-md-12">
                <strong>Confirm Password:</strong>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
				<span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                <!-- <input type="password" name="password1" value="{{ $user->password }}" class="form-control" placeholder="Password"> -->
            </div>
        </div>

        <button type="submit" class="btn btn-update btn-success col-sm-2">Update</button>
</div>
</form>
</div>
@endsection
