@extends('layouts.auth')

@section('content')

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
</head>
<style>
    .btn.btn-back {
        margin-top: 20px;
        margin-bottom: 15px;
        margin-left: 20px;
    }

    .header-text {
        margin-top: 10px;
        margin-left: 20px;
        margin-bottom: 5px;
        font-size: 30px;
        font-family: 'Raleway', sans-serif;
        font-weight: bold;
    }

    .btn-update {
        margin-left: 1px;
        margin-bottom: 40px;
    }

    hr {
        margin-bottom: 20px;
    }
</style>

<div class="container">
    <div class="row">
        <a class="btn btn-back btn-primary" href="{{ route('users.index') }}"> Back</a>
    </div>

    <div class="row">
        <h3 class="header-text"> Edit Member </h3>
    </div>

    <hr>

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form action="{{ route('users.update',$user->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="row">
            <div class="form-group col-md-12">
                <strong>Name:</strong>
                <input type="text" name="name" value="{{ $user->name }}" class="form-control" placeholder="Name">
				<span class="text-danger">{{ $errors->first('name') }}</span>
            </div>


            <div class="form-group col-md-12">
                <strong>Email:</strong>
                <input type="email" name="email" value="{{ $user->email }}" class="form-control" placeholder="Email">
				<span class="text-danger">{{ $errors->first('email') }}</span>
            </div>

        </div>

        <button type="submit" class="btn btn-update btn-success col-sm-2">Update</button>
</div>
</form>
</div>
@endsection
