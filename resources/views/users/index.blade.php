@extends('layouts.auth')

@section('content')
<head>
<link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
<style>
	.btn.btn-add {
		margin-top: 10px;
		margin-bottom: 15px;
		margin-left: 20px;
	}

	.btn.btn-action {
		margin-top: 5px;
		margin-bottom: 5px;
		margin-left: 20px;
	}

	.header-text {
		margin-top: 20px;
		margin-left: 35px;
		font-size: 25px;
		font-family: 'Raleway', sans-serif;
		font-weight: bold;
	}

	.table {
		margin-left: 20px;
		margin-right: 0;
		width: 100%;
	}

    th {
		font-family: 'Raleway', sans-serif;
	}

    .alert {
        margin-left: 20px;
        max-height: 50px;
    }

	.container{
		margin-left: 10px,
	}

</style>
</head>
<div class="container">
<div class="row">
	<h3 class="header-text"> Manage Members </h3>
</div>

<div class="row">
    <div class="col-lg-20 margin-tb">
        <div class="pull-right">
            <a class="btn btn-add btn-success" href="{{ route('users.create') }}"> + Add New Member</a>
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered table-hover">
	<thead>
    <tr>
        <th width="1%">#</th>
        <th width="20%">Name</th>
        <th width="20%">Email</th>
        <th width="30%">Action</th>
    </tr>
	</thead>
    @foreach ($users as $user)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email}}</td>
        <td>
            <form action="{{ route('users.destroy',$user->id) }}" method="POST">
  						<a class="btn btn-action btn-warning" href="{{ route('users.edit',$user->id) }}">Edit</a>


							<a class="btn btn-action btn-primary" href="{{ route('users.editpassword',$user->id) }}">Change Password</a>



                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-action btn-danger">Delete</button>


            </form>
        </td>
    </tr>
    @endforeach
</table>
</div>

{!! $users->links() !!}

@endsection
