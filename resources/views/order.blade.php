@extends('layout')

@section('title', 'Order')

@section('content')

<section class="section-pagetop bg-dark">
    <div class="container clearfix">
        <h2 class="title-page">Checkout</h2>
    </div>
</section>
<table id="cart" class="table table-hover table-condensed">
    <thead>
    <tr>
        <th style="width:50%">Product</th>
        <th style="width:10%">Price</th>
        <th style="width:8%">Quantity</th>
        <th style="width:22%" class="text-center">Subtotal</th>
        <th style="width:10%"></th>
    </tr>
    </thead>
    <tbody>


    <?php $total = 0 ?>

@foreach($orders as $order)

            <?php $total += $order['price'] * $order['quantity'] ?>



            <tr>
                <td data-th="Product">
                    <div class="row">

                        <div class="col-sm-3 hidden-xs"><img src="data:image/jpeg;base64,{{$order['image']}}" width="100" height="100" class="img-responsive"/></div>
                        <div class="col-sm-9">
                            <h4 class="nomargin" nema="name">{{ $order['name'] }}</h4>
                        </div>
                    </div>
                </td>
                <td data-th="Price">${{ $order['price'] }}</td>
                <td data-th="Quantity">
                  <td data-th="Price" name="quantity">{{ $order['quantity'] }}</td>

                </td>
                <td data-th="Subtotal" class="text-center">${{ $order['price'] * $order['quantity'] }}</td>

            </tr>
        @endforeach



    </tbody>

</table>

    <section class="section-content bg padding-y">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    @if (Session::has('error'))
                        <p class="alert alert-danger">{{ Session::get('error') }}</p>
                    @endif
                </div>
            </div>
<a href="{{ url('/cart') }}" class="btn btn-primary"><i class="fa fa-angle-left"></i> Back</a>
            <form action="{{ route('place.order') }}" method="POST" role="form">
                @csrf
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <header class="card-header">
                                <h4 class="card-title mt-2">Billing Details</h4>
                            </header>
                            <article class="card-body">
                                <div class="form-row">
                                    <div class="col form-group">
                                        <label>First name</label>
                                        <input type="text" class="form-control" name="first_name">
                                    </div>
                                    <div class="col form-group">
                                        <label>Last name</label>
                                        <input type="text" class="form-control" name="last_name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input type="text" class="form-control" name="address">
                                </div>

                                    <div class="form-group  col-md-6">
                                        <label>Phone Number</label>
                                        <input type="text" class="form-control" name="phone_number">
                                    </div>
                                </div>


                            </article>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <header class="card-header">
                                        <h4 class="card-title mt-2">Your Order</h4>
                                    </header>
                                    <article class="card-body">
                                        <dl class="dlist-align">
                                            <dt>Total cost: </dt>
                                            <dd class="text-right h5 b" name="grand_total"> {{ $total }} </dd>
                                        </dl>
                                    </article>
                                </div>
                            </div>
                            <div class="col-md-12 mt-4">

                                <button type="submit" class="subscribe btn btn-success btn-lg btn-block">Place Order</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@stop
