@extends('layout')

@section('content')
<head>
  <style>
  .table {
    margin-left: 5px;
    margin-top: 35px;
    width: 99%;
  }
  </style>
</head>
<div class="col-lg-12 margin-tb">
    <div class="pull-left">
        <h2>All Order</h2>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
<tr>
    <th>No</th>
    <th>Order ID</th>
    <th>Status</th>
    <th>Date</th>
    <th>Total</th>
    <th>Order Item(s)</th>
</tr>

@foreach ($orders as $order)
@if ($order-> user_id == Auth::id())
<tr>
    <td width="2%">{{$loop->iteration}}}</td>
    <td width="11%">{{ $order->order_number }}</td>
    <td width="5%">{{ $order->status }}</td>
    <td width="12%">{{ $order->transaction_date }}</td>
    <td width="8%">RM {{ $order->grand_total }}</td>
    <td width="8%">@foreach ( $order->items as $item) <li><a href={{ route('product.show',$item->product_id) }}>{{$item->product->name}} x {{$item->quantity}}</a></li> @endforeach</td>

</tr>
  @endif
@endforeach

</table>



@endsection
