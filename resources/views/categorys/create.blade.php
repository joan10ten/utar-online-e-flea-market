@extends('layouts.auth')

@section('content')
<head>
	<link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">

<style>
.btn.btn-back {
	margin-top: 20px;
	margin-bottom: 15px;
	margin-left: 20px;
}

.btn-add {
	margin-left: 13px;
	margin-bottom: 40px;
	height: auto;
}
hr {
			margin-bottom: 20px;
	}
</style>
</head>

<div class="container">
	<div class="row">
		<a class="btn btn-back btn-primary" href="{{ route('category.index') }}"> Back</a>
	</div>

	<div class="row">
		<h3 class="header-text"> Add New category </h3>
	</div>



	@if ($errors->any())
	<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
			</ul>
	</div>
	@endif

	<form action="{{ route('category.store') }}" method="POST" enctype="multipart/form-data">
	@csrf
	<hr>


		<div class="row">
			<div class="form-group col-md-12">
				<strong>Name:</strong>
				<input type="text" name="name" class="form-control" placeholder="Name" required focus>
				<span class="text-danger">{{ $errors->first('name') }}</span>
			</div>
			<button type="submit" class="btn btn-success ">Add</button>

		</div>
	</form>
</div>

@endsection
