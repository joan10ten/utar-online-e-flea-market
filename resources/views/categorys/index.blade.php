@extends('layouts.auth')

@section('content')
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Utar E-Flea Market Inventory Management System</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('category.create') }}"> + Create New category</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th width="280px">Action</th>
        </tr>

        @foreach ($categories->sortBy('name') as $category)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td><a href={{ route('category.show',$category->id) }}>{{ $category->name }}</a></td>
            <td>


                    <a class="btn btn-primary" href="{{ route('category.edit',$category->id) }}">Edit</a>


            </td>

        </tr>

        @endforeach

    </table>


@endsection
