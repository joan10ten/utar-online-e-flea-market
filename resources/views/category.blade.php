@extends('layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show category</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ url('product') }}"> Back</a>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Category:</strong>
                {{ $category->name }}
            </div>
        </div>
        @foreach ( $category->products as $product)
        @if ($product->unit > 0)
        <div class="col-xs-18 col-sm-6 col-md-3">
            <div class="thumbnail">
              <td width="1%"><img src="data:image/jpeg;base64,{{$product->image}}" width="150px" height="150px" /></td>
                <div class="caption">
                <h4><a href="{{route('product.show',$product->id) }}">{{ $product->name }}</a></h4>
                    <p><strong>Price: </strong> RM {{ $product->price }}</p>
                    <p><strong>Unit Left: </strong> {{ $product->unit }}</p>

                    <p><strong>Category: </strong> {{ $product->category -> name }}</p>
                    <p class="btn-holder"><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-warning btn-block text-center" role="button">Add to cart</a> </p>
                </div>
            </div>
        </div>
        @endif
        @endforeach


    </div>
@endsection
