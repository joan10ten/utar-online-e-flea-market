<?php
use App\Common;
?>
@extends('layout')

@section('title', 'Products')

@section('content')
<head>
<style>
.sidenav {
  width: 130px;
  position: fixed;
  z-index: 1;
  top: 20px;
  left: 10px;
  background: #eee;
  overflow-x: hidden;
  padding: 8px 0;
}

.sidenav a {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 15px;
  color: #2196F3;
  display: block;
}

.sidenav a:hover {
  color: #064579;
}


</style>

</head>


  <div class="sidenav">
    	<strong>Category</strong>
      @foreach($categories->sortBy('name') as $category)
      <a href="{{ route('categorys.show',$category->id) }}">{{ $category->name }}</a>

      @endforeach
  <a href="#about">About</a>
  <a href="#services">Services</a>
  <a href="#contact">Contact</a>
</div>

    <div class="container products">

        <div class="row">

            @foreach($products->sortByDesc('updated_at') as $product)
            @if ($product->unit > 0)
                <div class="col-xs-18 col-sm-6 col-md-3">
                    <div class="thumbnail">
                      <td width="1%"><img src="data:image/jpeg;base64,{{$product->image}}" width="150px" height="150px" /></td>
                        <div class="caption">
                        <h4><a href="{{route('product.show',$product->id) }}">{{ $product->name }}</a></h4>
                            <p><strong>Price: </strong> RM {{ $product->price }}</p>
                            <p><strong>Unit Left: </strong> {{ $product->unit }}</p>

                            <p><strong>Category: </strong> {{ $product->category -> name }}</p>
                            <p class="btn-holder"><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-warning btn-block text-center" role="button">Add to cart</a> </p>
                        </div>
                    </div>
                </div>
                  @endif
            @endforeach



        </div><!-- End row -->

    </div>

@endsection
@section('scripts')
<script type="text/javascript">

</script>
@endsection
