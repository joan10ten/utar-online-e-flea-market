<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <title>Utar E-Flea Market</title>

        <!-- Fonts -->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <!-- Styles -->
        <style>





              .top-right {
                  position: absolute;
                  right: 10px;
                  top: 10px;
              }



              .top-left {
                  position: absolute;
                  left: 10px;
                  top: 10px;
              }


              .links > a {
                  color: #636b6f;
                  padding: 0 25px;
                  font-size: 13px;
                  font-weight: 600;
                  letter-spacing: .1rem;
                  text-decoration: none;
                  text-transform: uppercase;
              }

        </style>
    </head>
    <body>

        <div class="flex-center position-ref full-height">
          <div class="top-left links"> <a href="{{ url('/home') }}"> Utar E-Flea Market </a> </div>
      @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else

                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))

                            <a href="{{ route('register') }}"> Register </a>

                        @endif
                    @endauth
                </div>
            @endif



        <main class="py-4">
            @yield('content')
        </main>
        @yield('scripts')
          </div>
    </body>
</html>
