@extends('layouts.auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    Hi boss!
                </div>
                <a href="{{ url('/users') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Manage Members</a>
                <a href="{{ url('/writers') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i>Manage Sellers</a>


            </div>
        </div>
    </div>
</div>
@endsection
