@extends('layouts.auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    Hi there, awesome seller
                </div>
                <a href="{{ url('/products') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue To Product Page</a>
                <a href="{{ url('/category') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue To Category Page</a>
                <a href="{{ url('/orders') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue To Order Page</a>

            </div>
        </div>
    </div>
</div>

@endsection
