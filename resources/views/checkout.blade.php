@extends('layout')

@section('title', 'Check Out')

@section('content')
<div class="checkout-panel">

  <div class="panel-body">
    <h2 class="title">Checkout here!</h2>

    <div class="progress-bar">
      <div class="step active"></div>
      <div class="step active"></div>
      <div class="step"></div>
      <div class="step"></div>
    </div>

    <div class="payment-method">
      <label for="card" class="method card">
        <div class="card-logos">
          <img src="https://designmodo.com/demo/checkout-panel/img/visa_logo.png"/>
          <img src="https://designmodo.com/demo/checkout-panel/img/mastercard_logo.png"/>
        </div>

        <div class="radio-input">
          <input id="card" type="radio" name="payment">
          Pay RM {{$order->grand_total}} with credit card
        </div>
      </label>

      <label for="paypal" class="method paypal">
        <img src="https://designmodo.com/demo/checkout-panel/img/paypal_logo.png"/>
        <div class="radio-input">
          <input id="paypal" type="radio" name="payment">
          Pay  RM {{$order->grand_total}} with PayPal
        </div>
      </label>
    </div>

    <div class="input-fields">
      <div class="column-1">
        <label for="cardholder">Name</label>
        <input type="text" id="cardholder" placeholder="e.g. Richard Bovell" />

        <div class="column-2">
          <label for="cardnumber">Card Number</label>
          <input type="password" id="cardnumber"/>

          <span class="info">* CVV or CVC is the card security code, unique three digits number on the back of your card separate from its number.</span>
        </div>
        <div class="small-inputs">
          <div>
            <label for="date">Expiry</label>
            <select id="expiry-date">
                <option>MM</option>
                <option value="1">01</option>
                <option value="2">02</option>
                <option value="3">03</option>
                <option value="4">04</option>
                <option value="5">05</option>
                <option value="6">06</option>
                <option value="7">07</option>
                <option value="8">08</option>
                <option value="9">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
            </select>
            <span>/</span>
             <select id="expiry-date">
                <option>YYYY</option>
                <option value="2020">2020</option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
                <option value="2023">2023</option>
                <option value="2024">2024</option>
                <option value="2025">2025</option>
                <option value="2026">2026</option>
                <option value="2027">2027</option>
                <option value="2028">2028</option>
                <option value="2029">2029</option>
                <option value="2030">2030</option>
                <option value="2031">2031</option>
            </select>
          </div>

          <div>
            <label for="verification">CVV / CVC *</label>
            <input type="password" maxlength="4" placeholder="123"/>
            <span class="info">* CVV or CVC is the card security code, unique three digits number on the back of your card separate from its number.</span>
          </div>
        </div>

      </div>

    </div>
  </div>
  <input name="payment_status" type="hidden" value="1">

  <div class="panel-footer">
    <a href="{{ url('/product') }}"><button class="btn back-btn">Back</button></a>
    <a href="{{ route('payment') }}"><button type="submit" class="btn next-btn"  >Confirm And Pay</button></a>
  </div>
</div>

@endsection
  @section('scripts')
  <script type="text/javascript">

  $(document).ready(function() {

  $('.method').on('click', function() {
    $('.method').removeClass('blue-border');
    $(this).addClass('blue-border');
  });

})
var $cardInput = $('.input-fields input');

$('.next-btn').on('click', function(e) {

  $cardInput.removeClass('warning');

  $cardInput.each(function() {
     var $this = $(this);
     if (!$this.val()) {
       $this.addClass('warning');
     }
  })
});
</script>
@endsection
