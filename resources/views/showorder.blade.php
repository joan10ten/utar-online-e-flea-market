@extends('layout')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Show Order</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('view.order') }}"> Back</a>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Item:</strong>
            {{ $order->items}}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Status:</strong>
            @foreach ( $order->items as $item) <li><a href={{ route('product.show',$item->product_id) }}>{{$item->product->name}} x {{$item->quantity}}</a></li> @endforeach
        </div>
    </div>

</div>
@endsection
