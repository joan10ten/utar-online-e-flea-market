@extends('layouts.auth')

@section('content')
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Utar E-Flea Market Inventory Management System</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('products.create') }}"> + Create New Product</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Image</th>
            <th>Price</th>
            <th>Unit</th>
            <th>Category</th>
            <th width="280px">Action</th>
        </tr>

        @foreach ($products->sortByDesc('updated_at') as $product)
        @if ($product-> writer_id == Auth::id())
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $product->name }}</td>
            <td width="1%"><img src="data:image/jpeg;base64,{{$product->image}}" width="150px" height="150px" /></td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->unit }}</td>
            <td width="8%"><a href={{ route('category.show',$product->category->id) }}>{{ $product->category -> name }}</a></td>
            <td>
                <form action="{{ route('products.destroy',$product->id) }}" method="POST">

                    <a class="btn btn-info" href="{{ route('products.show',$product->id) }}">Show</a>

                    <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}">Edit</a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
          @endif
        @endforeach

    </table>

    {!! $products->links() !!}

@endsection
