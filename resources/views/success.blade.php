@extends('layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                     Payment success! Thank You For shoppong With Us.
                </div>
                <a href="{{ url('/product') }}" class="btn btn-warning"><i class="fa fa-angle-right"></i> Continue Shopping</a>
            </div>
        </div>
    </div>
</div>
@endsection
