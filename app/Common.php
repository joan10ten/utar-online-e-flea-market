<?php

namespace App;

class Common
{
	public static $status = [
		'01' => 'Pending',
		'02' => 'Processing',
		'03' => 'Completed',
		'04' => 'Decline',

	];
	public static $paymentstatus = [
		'01' => 'Unpaid',
		'02' => 'Paid',
	

	];
}
