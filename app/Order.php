<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
      protected $table = 'orders';
      protected $fillable = [
      'order_number',  'user_id' ,'transaction_date' , 'status', 'grand_total', 'item_count', 'payment_status', 'payment_method',
        'first_name', 'last_name', 'address', 'phone_number'
    ];


      public function user()
      {
          return $this->belongsTo(User::class,'user_id');
      }

      public function products()
      {
          return $this->belongsToMany(Product::class, 'product_id');
      }
      public function items()
      {
    return $this->hasMany(OrderItem::class);
  }
}
