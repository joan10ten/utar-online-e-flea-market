<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{


      protected $fillable = [
          'name', 'price', 'unit', 'description', 'image','category_id',

      ];

      public function orders(){
            return $this->belongsToMany(Order::class);
        }
      /**
     * Get the category that the product belongs to.
     */
    public function category()
    {
        return $this->belongsTo(Category::class);

    }
    public function writer()
    {
        return $this->belongsTo(Writer::class);
    }
}
