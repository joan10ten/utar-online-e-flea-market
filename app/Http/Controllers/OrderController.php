<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
  public function index()
  {
    $orders = Order::all();

    return view('orders.index', compact('orders'));
  }


  public function show(Order $order)
  {

    return view('orders.show',compact('order'));

  }


  public function edit(Order $order)
  {


    return view('orders.edit', compact('order'));
  }


        public function update(Request $request, Order $order)
        {

          $request->validate([
              'status'=>'required',

          ]);
              $order->payment_status = $request->payment_status;
              $order->status = $request->status;

              $order->update();


            return redirect()->route('orders.index')
                            ->with('success','Order updated successfully');
        }

        public function destroy(Order $order)
        {
            $order->delete();

            return redirect()->route('orders.index')
                            ->with('success','Order deleted successfully');
        }


}
