<?php

namespace App\Http\Controllers;
use App\Category;
use Illuminate\Http\Request;

class CategorysController extends Controller
{
  /**
   * Display the specified resource.
   *
   * @param  \App\Category  $category
   * @return \Illuminate\Http\Response
   */
  public function show(Category $category)
  {
      return view('category',compact('category'));
  }
}
