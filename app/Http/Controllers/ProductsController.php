<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Order;
use App\OrderItem;
class ProductsController extends Controller
{
  public function index()
      {
        $products = Product::all();
        $categories = Category::all();

       return view('products',compact('products','categories'))
           ->with('i', (request()->input('page', 1) - 1) * 5);
      }
      public function home()
          {
            $products = Product::all();
            $categories = Category::all();

          return view('main', compact('products','categories'));
          }

      public function cart()
      {
          return view('cart');
      }
      public function addToCart($id)
      {
        $product = Product::find($id);

                if(!$product) {

                    abort(404);

                }

                $cart = session()->get('cart');

                // if cart is empty then this the first product
                if(!$cart) {

                    $cart = [
                            $id => [
                                "name" => $product->name,
                                "quantity" => 1,
                                "price" => $product->price,
                                "image" => $product->image
                            ]

                    ];

                    session()->put('cart', $cart);
                  //  DB::table('products')->decrement('unit', $cart[$id]['quantity']);
                  DB::table('products')->where('id',$product->id)->decrement('unit', 1);

                    return redirect()->back()->with('success', 'Product added to cart successfully!');
                }


                // if cart not empty then check if this product exist then increment quantity
                if(isset($cart[$id])) {

                    $cart[$id]['quantity']++;

                    session()->put('cart', $cart);
                    DB::table('products')->where('id',$product->id)->decrement('unit', 1);



                    return redirect()->back()->with('success', 'Product added to cart successfully!');

                }

                // if item not exist in cart then add to cart with quantity = 1
                $cart[$id] = [
                    "name" => $product->name,
                    "quantity" => 1,
                    "price" => $product->price,
                    "image" => $product->image
                ];

                session()->put('cart', $cart);
                DB::table('products')->where('id',$product->id)->decrement('unit', 1);

                return redirect()->back()->with('success', 'Product added to cart successfully!');
      }

      public function update(Request $request, Product $product)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');

            if($cart[$request->id]["quantity"]>$request->quantity){

              DB::table('products')->where('id',$request->id)->increment('unit', $cart[$request->id]["quantity"]-$request->quantity);

            }else {
              DB::table('products')->where('id',$request->id)->decrement('unit', $cart[$request->id]["quantity"]-$request->quantity);
            }
            $cart[$request->id]["quantity"] = $request->quantity;



            session()->put('cart', $cart);






            session()->flash('success', 'Cart updated successfully');

        }
    }

    public function remove(Request $request)
    {
        if($request->id) {

            $cart = session()->get('cart');

            if(isset($cart[$request->id])) {

              DB::table('products')->where('id',$request->id)->increment('unit',$cart[$request->id]["quantity"]);


                unset($cart[$request->id]);
                session()->put('cart', $cart);



            }

            session()->flash('success', 'Product removed successfully');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('details',compact('product'));
    }



        /**
         * Remove the specified resource from storage.
         *
         * @param  \App\Product  $product
         * @return \Illuminate\Http\Response
         */
        public function destroy(Product $product)
        {
            $product->delete();

            return redirect()->route('products.index')
                            ->with('success','Product deleted successfully');
        }

        public function order(Request $request)
        {
          if(! $request->session()->has('cart')) {
        return "stop no session";
            }

            return view('order')
              ->with('orders', $request->session()->get('cart'));
        }

        public function placeOrder(Request $request)
        {
          $count=0;
          $total = 0;
          $carts = session()->get('cart');
          foreach ($carts as $id => $details) {
              $count++;
              $total += $details['price'] * $details['quantity'];
          }



          $order = Order::create([
        'order_number'      =>  'ORD-'.strtoupper(uniqid()),
        'user_id'           => auth()->user()->id,
        'status'            =>  'pending',
        'grand_total'       =>  $total,
        'item_count'        =>  $count,
        'payment_status'    =>  0,
        'payment_method'    =>  null,
        'first_name'        =>  $request->first_name,
        'last_name'         =>  $request->last_name,
        'address'           =>  $request->address,
        'phone_number'      =>  $request->phone_number,
        'transaction_date' => now(),
    ]);

    if ($order) {

          $items = session()->get('cart');

        foreach ($items as $id => $details)
        {
            // A better way will be to bring the product id with the cart items
            // you can explore the package documentation to send product id with the cart
            $product = DB::table('products')->where('id',$request->id)->first();

            $orderItem = new OrderItem([
                'product_id'    =>  $id,
                'quantity'      =>  $details['quantity'],
                'price'         =>  $details['price'],
            ]);

            $order->items()->save($orderItem);
        }
    }


    //return $order;
      $carts = session()->forget('cart');

      return view('checkout', compact('order'));
        }
        public function vieworder(Request $request)
        {
            $orders = Order::all();
            return view('orderdetails', compact('orders'));

        }
        public function updateorder(Request $request,Order $order)
        {
          // DB::table('orders')->where('id',$request->id)->update(['payment_status' => 'true']);

          $order->payment_status =$request->payment_status  ;
          $order->update();
          return redirect()->view('payment');
      }


}
