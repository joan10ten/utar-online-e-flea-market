<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Auth;
use App\Product;
use App\Category;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $products = Product::latest()->paginate(5);

        return view('products.index',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        return view('products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:100',
            'price' => 'required',
            'description' => 'required|max:200',
            'category_id' => 'required|numeric',
            'image' => 'mimes:jpeg,jpg,png|required|image|max:600',
            // 'supplier_id' => 'required|numeric',
            'unit' => 'required|numeric',
        ]);

              $image = $request->file('image');
              $imagedata = file_get_contents($image);
              $base64 = base64_encode($imagedata);

              $product = new Product();
              $product->name = $request->name;
              $product->description = $request->description;
              $product->price = $request->price;
              $product->category_id = $request->category_id;
              $product->image = $base64;
              // $product->supplier_id = $request->supplier_id;
              $product->writer_id = Auth::id();
              $product->unit = $request->unit;

              $product->save();
        return redirect()->route('products.index')
                        ->with('success','Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
      // Check if user is the post author
        if ($product->writer_id === Auth::id()) {
            return view('products.show',compact('product'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
      if ($product->writer_id === Auth::id()) {
        $categories = Category::all();
      return view('products.edit', compact('product', 'categories'));
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
          'name' => 'required|max:100',
          'price' => 'required',
          'description' => 'required|max:200',
          'category_id' => 'required|numeric',
          'image' => 'mimes:jpeg,jpg,png|image|max:600',
          // 'supplier_id' => 'required|numeric',
          'unit' => 'required|numeric',
        ]);

        if ($request->file('image') != null) {

            $image = $request->file('image');
            $imagedata = file_get_contents($image);
            $base64 = base64_encode($imagedata);

            $product->name = $request->name;
            $product->description = $request->description;
            $product->category_id = $request->category_id;
            $product->price = $request->price;
            $product->image = $base64;
            // $product->supplier_id = $request->supplier_id;
            $product->unit = $request->unit;
            $product->update();
        } else {
            $product->update($request->all());
        }

        return redirect()->route('products.index')
                        ->with('success','Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('products.index')
                        ->with('success','Product deleted successfully');
    }
}
