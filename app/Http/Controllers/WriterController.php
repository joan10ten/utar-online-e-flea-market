<?php

namespace App\Http\Controllers;

use App\Writer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;


class WriterController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */


  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

    $writers = Writer::latest()->paginate(5);

    return view('writers.index', compact('writers'))
      ->with('i', (request()->input('page', 1) - 1) * 5);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('writers.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $request->validate([
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:writers',
      'password' => 'required|string|min:8|confirmed',
    ]);

    $writer = new Writer();
    $writer->name = $request->name;
    $writer->email = $request->email;
    $writer->password = Hash::make($request->password);
    $writer->save();

    return redirect()->route('writers.index')
      ->with('success', 'writer created successfully.');
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\writer  $writer
   * @return \Illuminate\Http\Response
   */
  public function show(Writer $writer)
  {
    return view('writers.show', compact('writer'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\writer  $writer
   * @return \Illuminate\Http\Response
   */
  public function edit(Writer $writer)
  {
    return view('writers.edit', compact('writer'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Writer  $writer
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Writer $writer)
  {
    $request->validate([
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255',
    ]);
    $writer->name = $request->name;
    $writer->email = $request->email;

    $writer->update();
    return redirect()->route('writers.index')
      ->with('success', 'writer updated successfully');
  }

  public function editpassword(Writer $writer)
  {
    return view('writers.editpassword', compact('writer'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\writer  $writer
   * @return \Illuminate\Http\Response
   */
  public function updatepassword(Request $request, Writer $writer)
  {
    $request->validate([
      'password' => 'required|string|min:8|confirmed',
    ]);
    $writer->password = Hash::make($request->password);

    $writer->update();
    return redirect()->route('writers.index')
      ->with('success', 'writer Password updated successfully');
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\writer $writer
   * @return \Illuminate\Http\Response
   */
   public function destroy(Writer $writer)
     {

       $writer->delete();

       return redirect()->route('writers.index')
         ->with('success', 'writer deleted successfully');
     }

}
