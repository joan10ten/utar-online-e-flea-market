<?php
Route::get('/', 'ProductsController@home');
Auth::routes();

Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm')->name('login.admin');
Route::get('/login/writer', 'Auth\LoginController@showWriterLoginForm')->name('login.writer');
Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm')->name('register.admin');
Route::get('/register/writer', 'Auth\RegisterController@showWriterRegisterForm')->name('register.writer');

Route::post('/login/admin', 'Auth\LoginController@adminLogin');
Route::post('/login/writer', 'Auth\LoginController@writerLogin');
Route::post('/register/admin', 'Auth\RegisterController@createAdmin')->name('register.admin');
Route::post('/register/writer', 'Auth\RegisterController@createWriter')->name('register.writer');

Route::view('/home', 'home')->middleware('auth');

Route::group(['middleware' => 'auth:admin'], function () {
    Route::view('/admin', 'admin');
    Route::resource('products','ProductController');
    Route::get('/users/{user}/editpassword', 'UserController@editpassword')->name('users.editpassword');
    Route::put('/users/{user}/updatepassword', 'UserController@updatepassword')->name('users.updatepassword');
    Route::resource('users', 'UserController');
    Route::get('/writers/{writer}/editpassword', 'WriterController@editpassword')->name('writers.editpassword');
    Route::put('/writers/{writer}/updatepassword', 'WriterController@updatepassword')->name('writers.updatepassword');
    Route::resource('writers', 'WriterController');



});

Route::group(['middleware' => 'auth:writer'], function () {
    Route::view('/writer', 'writer');
    Route::resource('products','ProductController');
    Route::resource('category','CategoryController');
    Route::resource('orders','OrderController');


});


Route::group(['middleware' => 'auth'], function () {
    Route::view('/home', 'home');
    Route::view('/checkout','checkout');
    Route::view('/payment','payment');
    Route::view('/success','success');


    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::post('/profile/update', 'ProfileController@updateProfile')->name('profile.update');

    //Route::resource('/maincategory','MainCategoryController');
    Route::resource('categorys','CategorysController');

    Route::get('cart', 'ProductsController@cart');




    Route::get('add-to-cart/{id}', 'ProductsController@addToCart');

    Route::patch('update-cart', 'ProductsController@update');

    Route::delete('remove-from-cart', 'ProductsController@remove');

    Route::get('/order', 'ProductsController@order')->name('orders');
    Route::post('/checkout/order', 'ProductsController@placeOrder')->name('place.order');
    Route::get('/orderdetails', 'ProductsController@vieworder')->name('view.order');

  //  Route::get('/showorder/{id}', 'ProductsController@showorder')->name('show.order');

    Route::get('/product', 'ProductsController@index');
    Route::patch('payment', 'ProductsController@updateorder')->name('payment');

    Route::resource('product','ProductsController');
    //Route::resource('order','OrderController');
    Route::get('/showorder/{id}', 'OrderController@show')->name('show.order');




});




Route::view('/logout','logout');
Route::view('/checkout','checkout');



Route::get('/profile', 'ProfileController@index')->name('profile');
Route::post('/profile/update', 'ProfileController@updateProfile')->name('profile.update');
